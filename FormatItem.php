<?php

#SYN:xpolas34

/**
 * Task SYN: Syntax highlighting in PHP 5 for IPP 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

/**
 * Class FormatItem represents a single formatting rule from format file.
 */
class FormatItem
{
    /**
     * FormatItem constructor.
     *
     * Setup formatting item using given information.
     *
     * @param string $regExp PHP style regular expression.
     * @param string $format One format item (divided by ',').
     */
    public function __construct($regExp, $format) {
        $this->mRegExp = $regExp;

        // Split value and format name
        $formatSplit = mb_split(":", $format);

        $this->mFormatName = $formatSplit[0];
        $this->mFormatVal = isset($formatSplit[1]) ? $formatSplit[1] : NULL;

        switch ($this->mFormatName) {
            case "bold" :
                $this->mBegTag = "<b>";
                $this->mEndTag = "</b>";
                break;
            case "italic" :
                $this->mBegTag = "<i>";
                $this->mEndTag = "</i>";
                break;
            case "underline" :
                $this->mBegTag = "<u>";
                $this->mEndTag = "</u>";
                break;
            case "teletype" :
                $this->mBegTag = "<tt>";
                $this->mEndTag = "</tt>";
                break;
            case "size" :
                // Value cannot be empty and has to be a valid number
                if (empty($this->mFormatVal) || !is_numeric($this->mFormatVal))
                {
                    fprintf(STDERR, "Error : size parameter is required and has to be a number!\n");
                    exit(ERR_FILE_FORMAT);
                }

                // Convert to integer
                $sizeVal = intval($this->mFormatVal);

                if ($sizeVal < 1 || $sizeVal > 7)
                {
                    fprintf(STDERR, "Error : size parameter has to be <1, 7>!\n");
                    exit(ERR_FILE_FORMAT);
                }

                $this->mBegTag = "<font size=".$sizeVal.">";
                $this->mEndTag = "</font>";
                break;
            case "color" :
                // Value cannot be empty, and has to be between <000000, FFFFFF>
                if (empty($this->mFormatVal) || strlen($this->mFormatVal) != 6 ||
                    !is_numeric("0x".$this->mFormatVal))
                {
                    fprintf(STDERR, "Error : color parameter is required to be <000000, FFFFFF>!\n");
                    exit(ERR_FILE_FORMAT);
                }

                // Test if the value is in the correct range
                $value = intval($this->mFormatVal, 16);
                if ($value < 0 || $value > intval("FFFFFF", 16))
                {
                    fprintf(STDERR, "Error : color parameter has to be <000000, FFFFFF>!\n");
                    exit(ERR_FILE_FORMAT);
                }

                $this->mBegTag = "<font color=#".$this->mFormatVal.">";
                $this->mEndTag = "</font>";
                break;
            default:
                fprintf(STDERR, "Error : Unknown format!\n");
                exit(ERR_FILE_FORMAT);
        } // switch end
    }

    /**
     * @return string PHP valid regular expression.
     */
    public function getRegExp()
    {
        return $this->mRegExp;
    }

    /**
     * @return string Readable name of the format.
     */
    public function getFormatName()
    {
        return $this->mFormatName;
    }

    /**
     * @return string Value in the format, it was found in the format file
     */
    public function getFormatVal()
    {
        return $this->mFormatVal;
    }

    /**
     * @return string Starting tag.
     */
    public function getBegTag()
    {
        return $this->mBegTag;
    }

    /**
     * @return string Ending tag.
     */
    public function getEndTag()
    {
        return $this->mEndTag;
    }

    ///PHP style regular expression
    private $mRegExp;

    ///Format name - bold, italic, underline, teletype, size or color
    private $mFormatName;

    ///Format value - depends on format type
    private $mFormatVal;

    ///Begin tag string
    private $mBegTag;

    ///End tag string
    private $mEndTag;
}

?>
