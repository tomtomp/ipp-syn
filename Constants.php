<?php

#SYN:xpolas34

/**
 * Task SYN: Syntax highlighting in PHP 5 for IPP 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

/**
 * This module contains return code constants.
 */

namespace cons;

define("ERR_OK", 0);
define("ERR_PARAM", 1);
define("ERR_FILE_READ", 2);
define("ERR_FILE_WRITE", 3);
define("ERR_FILE_FORMAT", 4);

?>
