<?php

#SYN:xpolas34

/**
 * Task SYN: Syntax highlighting in PHP 5 for IPP 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

require_once __DIR__ . "/FormatItem.php";
require_once __DIR__ . "/Constants.php";

/**
 * Class FormatFile
 *
 * Used for reading and processing of the format file.
 */
class FormatFile
{
    /**
     * Gets the content of given format file.
     *
     * @param string $formatFilename Filename of the format file. Must be valid!
     */
    public function __construct($formatFilename) {
        if (!FormatFile::$initialized)
            FormatFile::InitializeStatics();
        $this->mFormatContent = mb_split("\n",
            file_get_contents($formatFilename));
        $this->GenFormatArray();
    }

    /**
     * @return array Array of FormatItem objects.
     */
    public function getFormatArray() {
        return $this->mFormatArray;
    }

    /**
     * Generates array with FormatItem objects, according to the input file
     */
    private function GenFormatArray() {
        $arrayCounter = 0;
        $this->mFormatArray = array();

        foreach ($this->mFormatContent as $formatLine)
        {
            if (!empty($formatLine))
            {
                preg_match('/^([^\t]*)\t+(.*)$/', $formatLine, $matches);

                // There has to be at least 3 parts - the whole line, regular expression and format part
                if (count($matches) != 3)
                    continue;

                // Convert the regular expression to PHP regular expression.
                $regExp = $this->ConvertRegExp($matches[1]);

                // There may be more than one format type on one line, go through them all.
                $formats = mb_split("[ \t]*,[ \t]*", $matches[2]);
                foreach ($formats as $format)
                {
                    $this->mFormatArray[$arrayCounter++] = new FormatItem($regExp, $format);
                }
            }
        }
    }

    /**
     * Takes original regular expression, as defined in syn.pdf and ocnverts
     *   it to the php regular expression format.
     *
     * @param string $regExp The original regular expression.
     * @return string Php compatible regular expression.
     */
    private function ConvertRegExp($regExp) {
        $phpRegExp = $regExp;

        // Remove even number of negations.
        $phpRegExp = preg_replace('/(!!)+/', '', $phpRegExp);

        // Detect all of the disallowed combinations.
        if (preg_match('/(^\.)|(^\*)|(^\+)|(^\|)|((?<!%)!$)|((?<!%)%$)|((?<!%)\.$)|' .
            '((?<!%)\.\.)|((?<!%)\.\))|((?<!%)\(\.)|((?<!%)\(\+)|((?<!%)\(\*)|((?<!%)\.\*)|' .
            '((?<!%)\.\+)|((?<!%)\(\))|((?<!%)\|$)|((?<!%)%$)|((?<!%)\(\|)|((?<!%)\|\))|' .
            '((?<!%)\|\.)|((?<!%)\|\|)|((?<!%)!\.)|((?<!%)!\|)|((?<!%)!\*)|((?<!%)!\+)|' .
            '((?<!%)!\))|((?<!%)\.\|)/', $phpRegExp)) {
            fprintf(STDERR, "Error : Regular expression format is wrong!\n");
            exit(ERR_FILE_FORMAT);
        }

        // Replace **, *+, +* with one *
        // Replace ++ with one +
        $phpRegExp = preg_replace("/(?<!%)\\++/", "+", $phpRegExp);
        $phpRegExp = preg_replace("/(?<!%)\\*+/", "*", $phpRegExp);
        $phpRegExp = preg_replace("/(?<!%)(\\+\\*|\\*\\+)+(\\+|\\*)?/", "*", $phpRegExp);

        // Remove dots that mean concatenation.
        $phpRegExp = preg_replace('/([^%])\./', '$1', $phpRegExp);

        // Replace each character with escaped character
        $escapeChars = array (
            '\\', '$', '/', '^',
            '[', ']', '{', '}',
            '?'
        );
        foreach ($escapeChars as $escapeChar)
            $phpRegExp = str_replace($escapeChar, "\\".$escapeChar, $phpRegExp);

        // Negation with parentheses.
        while (preg_match("/(?<!%)!\\(/", $phpRegExp, $negMatches, PREG_OFFSET_CAPTURE)) {
            if (!isset($negMatches[0]))
                continue;

            $matchIndex = $negMatches[0][1];

            // Resulting regular expression from negation.
            $resRegex = "[^";

            // Skip the ! and ( .
            $index = $matchIndex + 2;
            $foundEnd = false;
            $escaped = false;
            $charsInSegment = 0;

            // Parse the negated expression.
            while ($index < strlen($phpRegExp) && !$foundEnd) {
                $currChar = $phpRegExp[$index];

                if ($escaped) {
                    // Some of the escaped sequences have a special meaning.
                    switch ($currChar) {
                        case "s":
                            $resRegex .= " \t\n\r\f\v";
                            break;
                        case "a":
                            $resRegex .= ".";
                            break;
                        case "d":
                            $resRegex .= FormatFile::$digits;
                            break;
                        case "l":
                            $resRegex .= FormatFile::$lowerLetters;
                            break;
                        case "L":
                            $resRegex .= FormatFile::$upperLetters;
                            break;
                        case "w":
                            $resRegex .= FormatFile::$lowerLetters . FormatFile::$upperLetters;
                            break;
                        case "W":
                            $resRegex .= FormatFile::$lowerLetters . FormatFile::$upperLetters . FormatFile::$digits;
                            break;
                        case "t":
                            $resRegex .= "\t";
                            break;
                        case "n":
                            $resRegex .= "\n";
                            break;
                        default:
                            $resRegex .= $currChar;
                            break;
                    }
                    $escaped = false;
                } else {
                    switch ($currChar) {
                        case "%":
                            $escaped = true;
                            break;
                        case ")":
                            $foundEnd = true;
                            break;
                        case "|":
                            $charsInSegment = 0;
                            break;
                        default:
                            if ($charsInSegment == 1) {
                                fprintf(STDERR, "Error : Regular expression format is wrong - negation" .
                                                " of multiple characters!\n");
                                exit(ERR_FILE_FORMAT);
                            }
                            $charsInSegment++;
                            $resRegex .= $currChar;
                            break;
                    }
                }

                $index++;
            }

            $resRegex .= "]";

            $phpRegExp = substr($phpRegExp, 0, $matchIndex) . $resRegex . substr($phpRegExp, $index);
        }

        // Negation with single character, or escaped character.
        $phpRegExp = preg_replace("/(?<!%)!([^%]|(?:%.))/", "[^$1]", $phpRegExp);

        $phpRegExp = str_replace("%%", "[%]", $phpRegExp);

        // Replace the special characters with correctly escaped versions
        $escapeChars = array (
            '.', '|', '!', '*',
            '+', '(', ')'
        );
        foreach ($escapeChars as $escapeChar)
            $phpRegExp = str_replace("%".$escapeChar, "\\".$escapeChar, $phpRegExp);

        // Replace the rest of the special combinations
        $phpRegExp = str_replace('%s', " |\t|\n|\r|\f|\v", $phpRegExp);
        $phpRegExp = str_replace('%a', ".", $phpRegExp);
        $phpRegExp = str_replace('%d', "\\d", $phpRegExp);
        $phpRegExp = str_replace('%l', "[a-z]", $phpRegExp);
        $phpRegExp = str_replace('%L', "[A-Z]", $phpRegExp);
        $phpRegExp = str_replace('%w', "[a-zA-Z]", $phpRegExp);
        $phpRegExp = str_replace('%W', "[a-zA-Z0-9]", $phpRegExp);
        $phpRegExp = str_replace('%t', "\t", $phpRegExp);
        $phpRegExp = str_replace('%n', "\n", $phpRegExp);

        if (preg_match("/(%[^\\]])|(%$)/", $phpRegExp)) {
            fprintf(STDERR, "Error : Regular expression format is wrong - unknown escape sequence!\n");
            exit(ERR_FILE_FORMAT);
        }

        // Add delimiters
        $phpRegExp = '/'.$phpRegExp.'/s';

        return $phpRegExp;
    }

    /**
     * Initialize all the static members.
     */
    static private function InitializeStatics() {
        FormatFile::$lowerLetters = implode(range('a', 'z'));
        FormatFile::$upperLetters = implode(range('A', 'Z'));
        FormatFile::$digits = implode(range('0', '9'));
        FormatFile::$whiteSpaces = " \t\n\r\f\v";
        FormatFile::$initialized = true;
    }

    ///When successfully constructed, will contain content of the format file.
    ///  Array, containing FormatItem objects.
    private $mFormatArray;

    ///Array containing formatting file split into lines.
    private $mFormatContent;

    /// Lower case letters in single string.
    static private $lowerLetters;
    /// Upper case letters in single string.
    static private $upperLetters;
    /// Digits in single string.
    static private $digits;
    /// Whitespaces in single string.
    static private $whiteSpaces;
    /// Are the static variables initialized?
    static private $initialized = false;
}
?>