<?php

#SYN:xpolas34

/**
 * Task SYN: Syntax highlighting in PHP 5 for IPP 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

/**
 * Class ParamProcessor
 *
 * Used for processing parameters.
 */
class ParamProcessor
{
    /**
     * ParamProcessor constructor.
     *
     * Automatically process input parameters.
     */
    public function __construct() {
        $this->mHelpOpt = false;
        $this->mBrOpt = false;

        $options = getopt("", self::$smParamArray);

        $this->ProcessParams($options);
    }

    /**
     * @return boolean
     */
    public function isSetHelp()
    {
        return $this->mHelpOpt;
    }

    /**
     * @return boolean
     */
    public function isSetBr()
    {
        return $this->mBrOpt;
    }

    /**
     * @return string
     */
    public function getFormatFN()
    {
        return $this->mFormatOpt;
    }

    /**
     * @return string
     */
    public function getInputFN()
    {
        return $this->mInputOpt;
    }

    /**
     * @return string
     */
    public function getOutputFN()
    {
        return $this->mOutputOpt;
    }

    /**
     * Process given options array, returned by getopt function
     *
     * @param array $options Options array returned by getopt function
     * @return NULL
     */
    private function ProcessParams($options) {

        $argNum = 0;

        if (isset($options["help"]))
        {
            $argNum++;
            if (is_array($options["help"]))
            {
                fprintf(STDERR, "ERROR : Only one help can be specified!\n");
                exit(ERR_PARAM);
            }
            $this->mHelpOpt = true;
            if (count($options) != 1)
            {
                fprintf(STDERR, "ERROR : help must be the only parameter!\n");
                exit(ERR_PARAM);
            }
        }
        else
        {
            if (isset($options["input"]))
            {
                $argNum++;
                if (is_array($options["input"]))
                {
                    fprintf(STDERR, "ERROR : Only one input can be specified!\n");
                    exit(ERR_PARAM);
                }
                $this->mInputOpt = $options["input"];
                if (!is_readable($this->mInputOpt))
                {
                    fprintf(STDERR, "Error : Input file is not readable!\n");
                    exit(ERR_FILE_READ);
                }
            }

            if (isset($options["output"]))
            {
                $argNum++;
                if (is_array($options["output"]))
                {
                    fprintf(STDERR, "ERROR : Only one output can be specified!\n");
                    exit(ERR_PARAM);
                }
                $this->mOutputOpt = $options["output"];
                try {
                    $outputFile = @fopen($this->mOutputOpt, "w");

                    if (!$outputFile)
                        throw new Exception();

                } catch (Exception $e) {
                    fprintf(STDERR, "Error : Output file is not writable!\n");
                    exit(ERR_FILE_WRITE);
                }
            }

            if (isset($options["format"]))
            {
                $argNum++;
                if (is_array($options["format"]))
                {
                    fprintf(STDERR, "ERROR : Only one format can be specified!\n");
                    exit(ERR_PARAM);
                }
                $this->mFormatOpt = $options["format"];
                if (!is_readable($this->mFormatOpt))
                {
                    $this->mFormatOpt = NULL;
                }
            }

            if (isset($options["br"]))
            {
                $argNum++;
                if (is_array($options["br"]))
                {
                    fprintf(STDERR, "ERROR : Only one br can be specified!\n");
                    exit(ERR_PARAM);
                }
                $this->mBrOpt = true;
            }
        }

        global $argc;
        if (count($options) != $argNum || $argc != $argNum + 1)
        {
            fprintf(STDERR, "ERROR : Unknown argument entered!\n");
            exit(ERR_PARAM);
        }
    }

    ///Parameter names, used in getopt
    private static $smParamArray = array(
        "help",
        "format:",
        "input:",
        "output:",
        "br",
    );

    ///Help option passed
    private $mHelpOpt;

    ///Option to add <br /> to the end of each line
    private $mBrOpt;

    ///Format filename, NULL means no format file
    private $mFormatOpt;

    ///Input filename. NULL means STDIN
    private $mInputOpt;

    ///Output filename. NULL means STDOUT
    private $mOutputOpt;
}

?>
