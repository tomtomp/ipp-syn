#!/usr/bin/php
<?php
#SYN:xpolas34

/**
 * Task SYN: Syntax highlighting in PHP 5 for IPP 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

require_once __DIR__ . "/FormatFile.php";
require_once __DIR__ . "/ParamProcessor.php";
require_once __DIR__ . "/Constants.php";

/**
 * Class Syntaxer
 *
 * Main application.
 */
class Syntaxer
{
    /**
     * Get program parameters and test their correctness.
     */
    public function __construct() {
        // Get and check parameters.
        $this->mParamProcessor = new ParamProcessor();
    }

    /**
     * Run the syntax highlighter.
     *
     * @return NULL
     */
    public function RunApp() {

        if ($this->mParamProcessor->isSetHelp())
        {
            printf("Usage : \n\t./syn.php <parameters>\n".
                   "Parameters can contain : \n".
                   "\t--help : Prints this help message. Cannot be combined with any other parameters.\n".
                   "\t--format=filename : Formatting file, has to be valid and can contain any number of \n".
                   "\t\tformatting records in this format : \n\t\t<regular expr>\t+<list of formats>.\n".
                   "\t\t<regular expr> specifies which parts of input text should be formatted \n".
                   "\t\t\tusing the <list of formats>. Operations are : \n".
                   "\t\tIf A and B are regular expressions, then - \n".
                   "\t\tA.B = concatenation of A and B. \n".
                   "\t\tAB  = short for A.B .\n".
                   "\t\tA|B = A or B.\n".
                   "\t\t!A  = negation of A.\n".
                   "\t\tA*  = any number (including 0 times) of A repeated after each other.\n".
                   "\t\tA+  = A repeated 1+ times after each other.\n".
                   "\t\t(A) = used for priority assertion.\n".
                   "\t\t\tSpecial combinations : \n".
                   "\t\t%%s  = white spaces ( \\t\\n\\r\\f\\v).\n".
                   "\t\t%%a  = any character.\n".
                   "\t\t%%d  = 0 - 9.\n".
                   "\t\t%%l  = a - z.\n".
                   "\t\t%%L  = A - Z.\n".
                   "\t\t%%w  = a - z + A - Z.\n".
                   "\t\t%%W  = a - z + A - Z + 0 - 9.\n".
                   "\t\t%%t  = \\t.\n".
                   "\t\t%%n  = \\n.\n".
                   "\t\t%%<special character>  = <special character> can be .|!*+()%% and results in \n".
                   "\t\t\tthe same character.\n".
                   "\t--input=filename : Input filename using UTF-8.\n".
                   "\t--output=filename : Output filename using UTF-8.\n".
                   "\t--br : Adds <br /> to each end of line.\n".
                   "If there was no input file passed, then the script uses STDIN instead.\n".
                   "If there was no output file passed, then the script uses STDOUT instead.\n".
                   "If no format file was specified, program returns the input text without any changes.\n");
        }
        else
        {
            if ($this->mParamProcessor->getInputFN())
                $inputText = file_get_contents($this->mParamProcessor->getInputFN());
            else
                $inputText = file_get_contents("php://stdin");

            if ($this->mParamProcessor->getFormatFN())
                $outputText = $this->ProcessInput($inputText);
            else
                $outputText = $inputText;

            if ($this->mParamProcessor->getOutputFN())
                file_put_contents($this->mParamProcessor->getOutputFN(), $outputText);
            else
                file_put_contents("php://stdout", $outputText);
        }

    }

    /**
     * Process given input, uses format file to add tags to the text.
     *
     * @param string $inputText Text to perform formatting on.
     * @return string Formatted text.
     */
    private function ProcessInput($inputText) {
        $myFormatFile = new FormatFile($this->mParamProcessor->getFormatFN());

        // Each element in this array will be copied before the index in final string.
        // Has one more element than inputText, because elements are copied before the character.
        $tagArray = array_fill(0, strlen($inputText) + 1, "");

        // Get tagging parameters
        $formatDirectives = $myFormatFile->getFormatArray();
        foreach ($formatDirectives as $directive)
        {
            try {
                if (@preg_match_all($directive->getRegExp(), $inputText,
                        $regExpMatches, PREG_OFFSET_CAPTURE) === false) {
                    fprintf(STDERR, "Error : regex is not correctly formulated!\n");
                    exit(ERR_FILE_FORMAT);
                }

                if (!isset($regExpMatches[0]))
                    continue;

                foreach ($regExpMatches[0] as $match)
                {
                    $matchLength = strlen($match[0]);
                    if ($matchLength == 0)
                        continue;

                    $begInd = $match[1];
                    $endInd = $begInd + $matchLength;

                    $tagArray[$begInd] = $tagArray[$begInd] . $directive->getBegTag();
                    $tagArray[$endInd] = $directive->getEndTag() . $tagArray[$endInd];
                }
            } catch (Exception $e) {
                fprintf(STDERR, "Error : regex is not correctly formulated!\n");
                exit(ERR_FILE_FORMAT);
            }

        } // foreach($formatDirectives...) end

        $outputText = "";

        // Compile the output text.
        for ($iii = 0; $iii < strlen($inputText); ++$iii)
        {
            $outputText .= $tagArray[$iii];
            $outputText .= $inputText[$iii];
        }

        $outputText .= $tagArray[strlen($inputText)];

        if ($this->mParamProcessor->isSetBr())
            $outputText = preg_replace("/\n/", "<br />\n", $outputText);

        return $outputText;
    }

    ///Used for storing parameter processor object.
    private $mParamProcessor;
}

$app = new Syntaxer();
$app->RunApp();

exit(ERR_OK);
?>
